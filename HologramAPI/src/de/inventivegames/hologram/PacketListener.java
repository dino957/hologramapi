/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package de.inventivegames.hologram;

import de.inventivegames.hologram.reflection.ClassBuilder;
import de.inventivegames.hologram.reflection.NMSClass;
import de.inventivegames.hologram.touch.TouchAction;
import de.inventivegames.hologram.touch.TouchHandler;
import de.inventivegames.hologram.view.ViewHandler;
import de.inventivegames.packetlistener.handler.PacketHandler;
import de.inventivegames.packetlistener.handler.PacketOptions;
import de.inventivegames.packetlistener.handler.ReceivedPacket;
import de.inventivegames.packetlistener.handler.SentPacket;
import de.inventivegames.packetlistener.reflection.AccessUtil;
import org.bukkit.plugin.Plugin;

import java.util.List;

public class PacketListener extends PacketHandler {

	protected static PacketListener instance;

	public PacketListener(Plugin pl) {
		super(pl);
		if (instance != null) { throw new IllegalStateException("Cannot instantiate PacketListener twice"); }
		instance = this;
		addHandler(instance);
	}

	public static void disable() {
		if (instance != null) {
			removeHandler(instance);
			instance = null;
		}
	}

	@SuppressWarnings({
			"rawtypes",
			"unchecked" })
	@Override
	@PacketOptions(forcePlayer = true)
	public void onSend(SentPacket packet) {
		if (!packet.hasPlayer()) { return; }
		int type = -1;
		if (packet.getPacketName().equals("PacketPlayOutSpawnEntityLiving")) {
			type = 0;
		}
		if (packet.getPacketName().equals("PacketPlayOutEntityMetadata")) {
			type = 1;
		}
		if (type == 0 || type == 1) {
			int a = (int) packet.getPacketValue("a");
			Object dataWatcher = type == 0 ? packet.getPacketValue("l") : null;

			if (dataWatcher != null) {
				try {
					dataWatcher = this.cloneDataWatcher(dataWatcher);// Clone the DataWatcher, we don't want to change the name values permanently
					AccessUtil.setAccessible(NMSClass.DataWatcher.getDeclaredField("a")).set(dataWatcher, null);
					packet.setPacketValue("l", dataWatcher);
				} catch (Exception e) {
					e.printStackTrace();
					return;// Allowing further changes here would mess up the packet values
				}
			}

			List list = (List) (type == 1 ? packet.getPacketValue("b") : null);
			int listIndex = -1;

			String text = null;
			try {
				if (type == 0) {
					text = (String) ClassBuilder.getWatchableObjectValue(ClassBuilder.getDataWatcherValue(dataWatcher, 2));
				} else if (type == 1) {
					if (list != null) {
						for (int i = 0; i < list.size(); i++) {
							int index = ClassBuilder.getWatchableObjectIndex(list.get(i));
							if (index == 2) {
								if (ClassBuilder.getWatchableObjectType(list.get(i)) == 4) {//Check if it is a string
									text = (String) ClassBuilder.getWatchableObjectValue(list.get(i));
									listIndex = i;
									break;
								}
							}
						}
					}
				}
			} catch (Exception e) {
				if (HologramAPI.useProtocolSupport) {
					e.printStackTrace();//Ignore the exception(s) when using protocol support
				}
			}

			if (text == null) {
				return;// The text will (or should) never be null
			}

			for (Hologram h : HologramAPI.getHolograms()) {
				if (((CraftHologram) h).matchesHologramID(a)) {
					for (ViewHandler v : h.getViewHandlers()) {
						text = v.onView(h, packet.getPlayer(), text);
					}
				}
			}

			if (text == null) {
				packet.setCancelled(true);//Cancel the packet if the text is null after calling the view handlers
				return;
			}

			try {
				if (type == 0) {
					ClassBuilder.setDataWatcherValue(dataWatcher, 2, text);
				} else if (type == 1) {
					if (list == null || listIndex == -1) { return; }
					Object object = ClassBuilder.buildWatchableObject(2, text);
					list.set(listIndex, object);
					packet.setPacketValue("b", list);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	@PacketOptions(forcePlayer = true)
	public void onReceive(ReceivedPacket packet) {
		if (packet.hasPlayer()) {
			if (packet.getPacketName().equals("PacketPlayInUseEntity")) {
				int id = (int) packet.getPacketValue("a");
				Object useAction = packet.getPacketValue("action");
				TouchAction action = TouchAction.fromUseAction(useAction);
				if (action == TouchAction.UNKNOWN) {
					return;// UNKNOWN means an invalid packet, so just ignore it
				}
				for (Hologram h : HologramAPI.getHolograms()) {
					if (((DefaultHologram) h).matchesTouchID(id)) {
						for (TouchHandler t : h.getTouchHandlers()) {
							t.onTouch(h, packet.getPlayer(), action);
						}
					}
				}
			}
		}
	}

	public Object cloneDataWatcher(Object original) throws Exception {
		if (original == null) { return original; }
		Object clone = NMSClass.DataWatcher.getConstructor(new Class[] { NMSClass.Entity }).newInstance(new Object[] { null });
		int index = 0;
		Object current = null;
		while ((current = ClassBuilder.getDataWatcherValue(original, index++)) != null) {
			ClassBuilder.setDataWatcherValue(clone, ClassBuilder.getWatchableObjectIndex(current), ClassBuilder.getWatchableObjectValue(current));
		}
		return clone;
	}

}
